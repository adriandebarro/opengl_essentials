#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <functional>

#include "../../Core/System/Common.h"


GLFWwindow* m_window;
using namespace std::placeholders;



//class KeyBoardHandler
//{
//public:
	static void KeyHandler(GLFWwindow *p_window, int p_keyPressed, int p_scanCode, int p_action, int mods)
	{
		if (p_keyPressed == GLFW_KEY_ESCAPE && p_action == GLFW_PRESS) {
			glfwDestroyWindow(p_window);
			glfwTerminate();
		}
	}
//};

typedef std::function<void(int, int, int, int)> KeyboardHandler;



int main()
{
	GLFWwindow* mainWindow = nullptr;
	GLuint shaderProgram;
	GLuint mVertexArrayObject;

	std::string vertexShaderPath = "/home/adrian/ClionProjects/OpenGLSaf/Src/Lessons/Lesson2/vertexShader.vert";
	std::string fragmentShaderPath = "/home/adrian/ClionProjects/OpenGLSaf/Src/Lessons/Lesson2/fragmentShader.frag";

	//KeyBoardHandler* myHandler = new KeyBoardHandler();

//	KeyboardHandler* keyboardFunctionPointer = nullptr;
//	keyboardFunctionPointer = std::bind(&KeyBoardHandler::KeyHandler, myHandler, _1, _2, _3, _4,_5);

	if(glfwInit() == GLFW_FALSE)
	{
		std::cout << "Cannot intialise GLFW" << std::endl;
		return -1;
	}

	m_window = glfwCreateWindow(500, 500, "Lesson 2", nullptr, nullptr);


	glfwMakeContextCurrent(m_window);

	if(glewInit() == GL_FALSE)
	{
		std::cout << "Failed to initialise glew" <<std::endl;
	}



	glfwSetKeyCallback(m_window, KeyHandler);


	glClearColor(1, 0,1 , 1);

	GLuint success = GL_FALSE;
	shaderProgram = Core::Common::OpenglUtil::LoadShaders(vertexShaderPath, fragmentShaderPath, &success);

	if(success == GL_TRUE) {

		glGenVertexArrays(1, &mVertexArrayObject);

		do {
			glClear(GL_COLOR_BUFFER_BIT);

			glBindVertexArray(mVertexArrayObject);
			glUseProgram(shaderProgram);
			glPointSize(80.0f);
			glDrawArrays(GL_POINTS, 0,1);
			glBindVertexArray(0);
			glfwSwapBuffers(m_window);
			glfwPollEvents();

		} while (!glfwWindowShouldClose(m_window));

	}
	//delete myHandler;

	return 0;
}
