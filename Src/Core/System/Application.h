#pragma once 

#include "./Common.h"

namespace OpenGLSaf
{
	namespace Core
	{
		class Application
		{

		public:
			virtual void InitScreen() = 0;
			virtual void ExecuteApplication();
			virtual void Terminate() = 0;
		protected:
			virtual void ExecuteLoop() = 0;
		};
	}
}
