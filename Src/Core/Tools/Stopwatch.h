#include <chrono>
#include <iostream>



namespace Core
{
	namespace Tools
	{
		/*
		 * The class Stopwatch is a construct that takes the elasped time in a scope
		 * using the
		 * */
		template<typename T>
		class StopWatch
		{
		private:
			std::chrono::system_clock::time_point m_start;
			std::string m_stopwatchName;

		public:
			StopWatch(std::string p_stopwatchName):
				m_start(std::chrono::system_clock::time_point::min()),
				m_stopwatchName(p_stopwatchName)

			{
				Start();
			}

			void ResetTimer()
			{
				m_start = std::chrono::system_clock::time_point::min();
			}


			void Start()
			{
				m_start = std::chrono::system_clock::now();
			}


			bool IsStarted()
			{
				return (m_start != std::chrono::system_clock::time_point::min());
			}

			unsigned long GetMs()
			{
				if(IsStarted())
				{
					std::chrono::system_clock::duration diff;
					diff = std::chrono::system_clock::now() - m_start;
					return (std::chrono::duration_cast<T>(diff).count());
				}
			}

			~StopWatch()
			{
				unsigned long diff = GetMs();
				std::cout << m_stopwatchName<<" Time taken is : " <<  diff  << "Ms"<< std::endl;
			}

		};
	}
}
