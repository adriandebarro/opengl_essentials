///--------------------------------------------------------------------------------------------
/// <file>  RenderingApplication.cpp  </file>
/// <author> Adrian De Barro </author>
/// <date> 21/09/2017 </date>
/// <remark>
///     Provides a structurted component based life Cycle framework for an OpenGL application
/// </remark>
///--------------------------------------------------------------------------------------------

#include "./RenderingApplication.h"

using namespace OpenGLSaf::Core::System;

RenderingApplication *RenderingApplication::s_internalInstance;


///--------------------------------------------------------------------------------------------

RenderingApplication::RenderingApplication(int p_width, int p_height, std::string p_windowName)
        : m_screenWidth(p_width),
          m_screenHeight(p_height),
          m_windowName(p_windowName) { }

///--------------------------------------------------------------------------------------------

void RenderingApplication::Run() {
    InitialiseWindow();
    InitialiseOpenGL();
    Initialise();

    do {
        Update(m_renderingTime);
        Draw(m_renderingTime);
        glfwPollEvents();

    } while (!glfwWindowShouldClose(m_window));

    ShutDown();
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::ShutDown() {
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::InitialiseWindow() {
    if (glfwInit() == GLFW_FALSE) {
        std::cout << "Failed to initialise GLFW" << std::endl;
        return;
    }

    m_window = glfwCreateWindow(m_screenWidth, m_screenHeight, m_windowName.c_str(), nullptr, nullptr);

    if (m_window != nullptr) {
        ShutDown();
        std::cout << "Window Initialised" << std::endl;
    }

    Implement("Implement center winod functionality");

    //TODO center window
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::CentreWindow() {
    Implement("CentreWindow");
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::Initialise() {
    for (RenderingComponent *currentComponent : m_renderingComponents) {
        currentComponent->Initialise();
    }
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::Update(const RenderingTime &p_time) {
    for (RenderingComponent *currentComponent : m_renderingComponents) {
        if (currentComponent->Enabled()) {
            currentComponent->Update(p_time);
        }
    }

}

///--------------------------------------------------------------------------------------------

void RenderingApplication::Draw(const RenderingTime &p_gameTime) {
    for (RenderingComponent *currentComponent : m_components) {
        DrawableRenderingComponent *drawableComponent = currentComponent->As<DrawableRenderingComponent>();

        if (drawableComponent != nullptr && drawableComponent->Visible()) {
            drawableComponent->Draw(m_renderingTime);
        }
    }


}

///--------------------------------------------------------------------------------------------

void RenderingApplication::InitialiseOpenGL() {
    this->s_internalInstance = this;
    //setting out window as the current context
    glfwMakeContextCurrent(m_window);
    if (glewInit() == GL_FALSE) {
        std::cout << "Failed to initalise OpenGl" << std::endl;
    }

    //setting the major and minor versions of opengl
    glGetIntegerv(GL_MAJOR_VERSION, &m_openGlMajorVer);
    glGetIntegerv(GL_MINOR_VERSION, &m_openGlMinorVer);

    //setting the current window to fullscreen
    glViewport(0, 0, m_screenWidth, m_screenWidth);

    glfwSetKeyCallback(m_window, RenderingApplication::OnKey);

    if (m_depthStencileBufferEnabled) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_EQUAL);
    }
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::OnKey(GLFWwindow *p_window, int key, int scancode, int action, int mode) {
    // c++11 syntax (foreach loop kind of) this is an iterator
    for (auto currentHandler : RenderingApplication::s_internalInstance->m_keyboardHandlers) {
        currentHandler.second(key, scancode, action, mode);
    }
}

///--------------------------------------------------------------------------------------------

void RenderingApplication::AddKeyHandler(KeyboardHandler p_handler) {
    m_keyboardHandlers[&p_handler] = p_handler;
}

///--------------------------------------------------------------------------------------------