#pragma once 


#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>

#include <GL/glew.h>
#include <glm/vec4.hpp>
#include <GLFW/glfw3.h>


namespace Core {

    namespace Common {

        namespace Logging
        {
            static void Logging(std::string p_message) {
                std::cout << p_message << std::endl;
            };
        }

        namespace SystemUtil
        {
            static void Pause()
            {
                std::cout << "Press any key to continue..." << std::endl;
                std::cin.get();
            };

            #define Implement(value) std::cout << value << "Has not yet been implemented" << std::endl; std::cin.get();

        }


        namespace StringUtil
        {

            static std::string ReadFile(std::string p_filePath)
            {
                std::stringstream output;

                std::ifstream file(p_filePath);
                std::string currentLine = "";

                while (std::getline(file, currentLine)) {
                    output << currentLine << "\n";
                }

                return output.str();
            };
        }


        namespace OpenglUtil
        {


            enum ErrorCheckType {
                SHADER_CHECK = 0,
                PROGRAM_CHECK
            };

            static int CheckForErrors(const int *p_logLength, const GLuint *p_Id, ErrorCheckType p_errorCheckType)
            {
                if ((*p_logLength) > 0)
                {
                    std::vector<char> vectorErrorMessage((*p_logLength) + 1);

                    switch (p_errorCheckType) {
                        case ErrorCheckType::SHADER_CHECK:
                            glGetShaderInfoLog(*p_Id, *p_logLength, nullptr, &vectorErrorMessage[0]);
                            break;

                        case ErrorCheckType::PROGRAM_CHECK:
                            glGetProgramInfoLog(*p_Id, *p_logLength, nullptr, &vectorErrorMessage[0]);
                            break;

                        default:
                            break;
                    }

                    for (std::vector<char>::iterator iter = vectorErrorMessage.begin(); iter != vectorErrorMessage.end(); iter++)
                        std::cout << *iter;

                    std::cout << std::endl;
                    return GL_FALSE;
                }

                return GL_TRUE;
            };

            /*
             * Compiles any shader
             * <args>
             *      char* p_shaderPtr: pointer to the char array conatining the pointer
             *      GLuint* p_shaderId: pointer which will contain the id asisgned to the shader
             * </args>
             * <returns>
             *      GLunit: returns a success or a failure
             * </returns>
             * */
            static GLuint CompileShader(const char *p_shaderPtr, GLuint *p_shaderId)
            {
                int infoLogLength;
                GLint result = GL_FALSE;
                glShaderSource(*p_shaderId, 1, &p_shaderPtr, nullptr);
                glCompileShader(*p_shaderId);

                glGetShaderiv(*p_shaderId, GL_COMPILE_STATUS, &result);
                glGetShaderiv(*p_shaderPtr, GL_INFO_LOG_LENGTH, &infoLogLength);

                result = CheckForErrors(&infoLogLength, p_shaderId, ErrorCheckType::SHADER_CHECK);
                return result;
            };
            //-------------------------------------------------------------------------------------------------

            /*
             * Compiles any shader
             * <args>
             *      char* p_shaderPtr: pointer to the char array conatining the pointer
             *      GLuint* p_shaderId: pointer which will contain the id asisgned to the shader
             * </args>
             * <returns>
             *      GLunit: returns a success or a failure
             * </returns>
             * */
            static GLvoid PreCreatedProgramLoadShaders(std::string p_vertexShader, std::string p_fragmentShader,
                                                       GLuint *p_programID, GLuint *p_success) {
                //create the shaders
                GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
                GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

                //loading the string data fo the shaders
                std::string vertexShader = StringUtil::ReadFile(p_vertexShader);
                std::string fragmentShader = StringUtil::ReadFile(p_fragmentShader);

                GLint result = GL_FALSE;
                int infoLogLength;

                if (vertexShader != "" && fragmentShader != "") {
                    std::cout << "Compiling vertex shader data: " << std::endl;

                    //compile the vertex shader
                    char const *vertexSourcePtr = vertexShader.c_str();
                    result = CompileShader(vertexSourcePtr, &vertexShaderID);

                    //compile the fragment shader
                    char const *fragmentShaderPte = fragmentShader.c_str();
                    result = CompileShader(fragmentShaderPte, &fragmentShaderID);


                    glAttachShader(*p_programID, vertexShaderID);
                    glAttachShader(*p_programID, fragmentShaderID);
                    glLinkProgram(*p_programID);

                    glGetProgramiv(*p_programID, GL_LINK_STATUS, &result);
                    glGetProgramiv(*p_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
                    result = CheckForErrors(&infoLogLength, p_programID, ErrorCheckType::PROGRAM_CHECK);

                    glDetachShader(*p_programID, vertexShaderID);
                    glDetachShader(*p_programID, fragmentShaderID);

                    glDeleteShader(vertexShaderID);
                    glDeleteShader(fragmentShaderID);

                    *p_success = GL_TRUE;
                }
                else {
                    std::cout << "Vertex and Segment shaders were not loaded properly" << std::endl;
                    *p_success = GL_FALSE;
                }
            };

            static GLuint LoadShaders(std::string p_vertexShader, std::string p_fragmentShader, GLuint *p_success) {
                //create the shaders
                GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
                GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

                //loading the string data fo the shaders
                std::string vertexShader = StringUtil::ReadFile(p_vertexShader);
                std::string fragmentShader = StringUtil::ReadFile(p_fragmentShader);

                GLint result = GL_FALSE;
                int infoLogLength;

                if (vertexShader != "" && fragmentShader != "") {
                    std::cout << "Compiling vertex shader data: " << std::endl;

                    //compile the vertex shader
                    char const *vertexSourcePtr = vertexShader.c_str();
                    result = CompileShader(vertexSourcePtr, &vertexShaderID);

                    if (result == GL_TRUE) {
                        std::cout << "Vertex Shader compiled succesfully" << std::endl;
                    }
                    else{
                        std::cout << "Vertexc shader was not compiled properly" <<std::endl;
                    }

                    //compile the fragment shader
                    char const *fragmentShaderPte = fragmentShader.c_str();
                    result = CompileShader(fragmentShaderPte, &fragmentShaderID);

                    if (result == GL_TRUE) {
                        std::cout << "Fragment Shader compiled succesfully" << std::endl;
                    }
                    else{
                        std::cout << "Fragment shader not compiled successfully " << std::endl;
                    }

                    GLuint programID = glCreateProgram();

                    glAttachShader(programID, vertexShaderID);
                    glAttachShader(programID, fragmentShaderID);
                    glLinkProgram(programID);

                    glGetProgramiv(programID, GL_LINK_STATUS, &result);
                    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
                    result = CheckForErrors(&infoLogLength, &programID, ErrorCheckType::PROGRAM_CHECK);

                    glDetachShader(programID, vertexShaderID);
                    glDetachShader(programID, fragmentShaderID);

                    glDeleteShader(vertexShaderID);
                    glDeleteShader(fragmentShaderID);

                    *p_success = GL_TRUE;
                    return programID;
                }
                else {
                    std::cout << "Vertex and Segment shaders were not loaded properly" << std::endl;
                    *p_success = GL_FALSE;
                    return -1;
                }
            };
        }
    }
}
