///--------------------------------------------------------------------------------------------
/// <file>  RenderingApplication.cpp  </file>
/// <author> Adrian De Barro </author>
/// <date> 21/09/2017 </date>
/// <remark>
///     Provides a structurted component based life Cycle framework for an OpenGL application
/// </remark>
///--------------------------------------------------------------------------------------------

#include <iostream>
#include <functional>
#include <map>
#include <vector>
#include <string>

#include "./Common.h"

namespace OpenGLSaf
{
	namespace Core
	{
		namespace System
		{
			/*
			 *
			 */
			class RenderingApplication
			{
			public:
				/*
				 *	KeyboardHandler:  a function wrapper for a function with 4 integer parameters
				 *					  All Keyboard handlers must make use of the followiong format
				 */
				typedef std::function<void(int, int, int, int)> KeyboardHandler;

				/*
				 *	Constructor for a Rendering Applica tion
				 *	@Param p_width: Input parameter for nthe width of the viewing window
				 *	@Param p_height: Input parameter for the height of the viewing window
				 *	@Param p_title: Input parameter for the title of the viewing window
				 */
				RenderingApplication(int p_width, int p_height, std::string p_title);

				/*
				 *	Invokes the standard exuecution procedure for the rendering application
				 */
				virtual void Run();

				/*
				 *	Adds a keyboard handler to the map of keyboard handler
				 *	@Param p_handler: The new keyboard handler to be added to the list
				 */
				void AddKeyHandler(KeyboardHandler p_handler);

				/*
				 *	Deletes a specific keyboardhandler from the list
				 *	@Param p_handler: The keybaordhandler function pointer
				 */
				void DeleteKeyHandler(KeyboardHandler p_handler);

				/*
				 *	Loops through all registered function pointers to handle an incoming command
				 *	The format of the OnKey function follows the format of thej GLFW
				 */
				static void OnKey(GLFWwindow *, int, int, int, int);

				/*
				 * Static instance of the RenderingApplication for the OnKey static method
				 * This pointer is set in the InitialiseOpenGL method
				 */
				static RenderingApplication* s_internalInstance;



			protected:

				/*
				 *	Initialises the GLFW window/s
				 * 	This method can be overrriden and changed as required
				 */
				virtual void InitialiseWindow();
				/*
				 *	Initialises OpenGL and binds the window to the current context
				 *	This method can be overriden and changed as required
				 */
				virtual void InitialiseOpenGL();
				/*
				 * 	Initialises any required rendering components
				 */
				virtual void Initialise();
				/*
				 * 	Loops through all the component instances and Executes the draw
				 * 	function of any components that are of type DrawabelRenderingComponent
				 */
				virtual void Draw(const RenderingTime&);
				/*
				 * 	Loops through all the component instances and Executes the Update function for any enabled components
				 */
				virtual void Update(const RenderingTime&);
				/*
				 * Terminates the GLFW window and removes and allocatend data
				 */
				virtual void ShutDown();

			private:

				void CentreWindow();

				GLFWwindow *m_window;
				std::string m_windowName;
				int m_screenWidth;
				int m_screenHeight;

				int m_openGlMajorVer;
				int m_openGlMinorVer;

				RenderingTime m_renderingTime;
				std::vector<RenderingComponent*> m_renderingComponents;

				std::map<KeyboardHandler *, KeyboardHandler> m_keyboardHandlers;
				std::vector<RenderingComponent*> m_components;

				bool m_depthStencileBufferEnabled = false;

			};
		}
	}
}
