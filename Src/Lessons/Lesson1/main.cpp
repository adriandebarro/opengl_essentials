#include <iostream>

#include <GL/glew.h>
#include <glm/vec4.hpp>
#include <GLFW/glfw3.h>


/**
 * Lesson 1:
 * Init GLEW
 * Init GLFW
 * OpenWindow
 * SetColor
 * PollEvents
 * Terminate
 */
int main(void)
{
	glm::vec4 cornflowerBlue = glm::vec4(0.392f, 0.584f, 0.929f, 1.0f);

	std::cout << "Going to initialise OpenGL and GLFW" << std::endl;

	//init glfw
	if(!glfwInit())
	{
		std::cout << "Failed to initialise GLFW" << std::endl;
		return -1;
	}

	//create a window and bind it to the handle
	GLFWwindow* window = glfwCreateWindow(800, 600, "Lesson 1 window", nullptr, nullptr);

	//initialise GLEW
	if(glewInit() == GL_FALSE)
	{
		std::cout <<  "Failed to initialise GLEW" << std::endl;
		return -1;
	}

	//never make context before init glew
	glfwMakeContextCurrent(window);

	//size of the window
	glViewport(0,0,800, 600);
	//set clear colort
	glClearColor(0.392f, 0.584f, 0.929f, 1.0f);

	do
	{
		//clear buffer and set to the clear color
		glClear(GL_COLOR_BUFFER_BIT);
		//swap the buffer (2 way buffer)
		glfwSwapBuffers(window);
		//listen for new events
		glfwPollEvents();
	}
	while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS);

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
