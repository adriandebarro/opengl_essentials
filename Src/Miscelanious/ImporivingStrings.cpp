//
//namespace OpenGlSaf
//{
//	namespace Lesson2
//	{
//		class Lesson2App : public application
//		{
//			Lesson2App(int, int);
//			virtual void
//		};
//	}
//}


//std::string RemoveCtrl(std::string p_stringContent)
//{
//	std::string result;
//
//	for(int index = 0; index < p_stringContent.length(); index++)
//	{
//		if(p_stringContent[index] >= 0x20)
//			result += p_stringContent[index];
//
//	}
//
//	return result;
//}
//
//std::string RemoveCtrlReserve(std::string p_stringContent)
//{
//	std::string outputString;
//	outputString.reserve(p_stringContent.length());
//
//	for(int index =0; index < p_stringContent.length(); index++)
//	{
//		if(p_stringContent[index] >= 0x20)
//			outputString += p_stringContent[index];
//	}
//
//	return outputString;
//}
//
//
///*
// * In this example we are improving o nthe example created before it
// * We are not editing the value of the argument thus if we pass it as const we dont have to pass it by value
// *
// * However there is a bummer with this code.....
// *
// * reference variables are passed as a pointer, therefore everywhere s is used the pointer is dereferenced
// * and we dont want that
// * */
//std::string RemoveCtrlConst(std::string const &p_stringContent)
//{
//	std::string outputString;
//	outputString.reserve(p_stringContent.length());
//
//	for(int index =0; index < p_stringContent.length(); index++)
//	{
//		if(p_stringContent[index] >= 0x20)
//			outputString += p_stringContent[index];
//	}
//
//	return outputString;
//}
//
//
///*
// * Improving on the issue of dereferincing
// *
// *
// * */
//std::string RemoveCtrlIterator(std::string const &p_stringContent)
//{
//	std::string outputString;
//	outputString.reserve(p_stringContent.length());
//
//	for(auto it = p_stringContent.begin(), end = p_stringContent.end(); it != end;++it)
//	{
//		if(*it >= 0x20)
//			outputString += *it;
//	}
//
//	return outputString;
//}
//
//
//
///*
// * Removing the burden of allocation in the loop
// *
// *
// * */
//std::string RemoveCtrlNoStringCopy(std::string &p_outputString, std::string const &p_stringContent)
//{
//	p_outputString.clear();
//	p_outputString.reserve(p_stringContent.length());
//
//	for(auto it = p_stringContent.begin(), end = p_stringContent.end(); it != end;++it)
//	{
//		if(*it >= 0x20)
//			p_outputString += *it;
//	}
//}
//
//int main()
//{
//
////	if(!glfwInit())
////	{
////		std::cout << "GLFW not able to init" << std::endl;
////	}
//
//
//
//
//	{
//		Core::Tools::StopWatch<std::chrono::nanoseconds> testStopwatch("NormalStopwatch");
//		for (int index = 0; index < 500; index++)
//		{
//			std::string myString = RemoveCtrl(
//					"This is a test that i will try to carry one and i need a looonngggggg sentence top test how much time will this function take");
//		}
//
//		float callCost = testStopwatch.GetMs()/ 200;
//		std::cout << "Cost per call: " << callCost << std::endl;
//	}
//
//	{
//		Core::Tools::StopWatch<std::chrono::nanoseconds> testStopwatch("Reserve length of output string");
//		for (int index = 0; index < 500; index++)
//		{
//			std::string myString = RemoveCtrlReserve(
//					"This is a test that i will try to carry one and i need a looonngggggg sentence top test how much time will this function take");
//		}
//		float callCost = testStopwatch.GetMs()/ 200;
//		std::cout << "Cost per call: " << callCost << std::endl;
//	}
//
//	{
//		std::string test = "This is a test that i will try to carry one and i need a looonngggggg sentence top test how much time will this function take";
//		Core::Tools::StopWatch<std::chrono::nanoseconds> testStopwatch("Set as const and push as an array");
//		for (int index = 0; index < 500; index++)
//		{
//			std::string myString = RemoveCtrlConst(test);
//		}
//		float callCost = testStopwatch.GetMs()/ 200;
//		std::cout << "Cost per call: " << callCost << std::endl;
//	}
//
//
//	{
//		Core::Tools::StopWatch<std::chrono::nanoseconds> testStopwatch("FourthStopwatch");
//		for (int index = 0; index < 500; index++)
//		{
//			std::string myString = RemoveCtrlIterator("This is a test that i will try to carry one and i need a looonngggggg sentence top test how much time will this function take");
//		}
//		float callCost = testStopwatch.GetMs()/ 200;
//		std::cout << "Cost per call: " << callCost << std::endl;
//	}
//
//	{
//		Core::Tools::StopWatch<std::chrono::nanoseconds> testStopwatch("FifthStopwatch");
//		for (int index = 0; index < 500; index++)
//		{
//			std::string myString;
//			RemoveCtrlNoStringCopy(myString,
//					"This is a test that i will try to carry one and i need a looonngggggg sentence top test how much time will this function take");
//		}
//		float callCost = testStopwatch.GetMs()/ 200;
//		std::cout << "Cost per call: " << callCost << std::endl;
//	}
//
//
//	return 0;
//}

