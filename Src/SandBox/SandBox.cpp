#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <map>
#include <functional>
#include <vector>
#include <string>
#include <bits/unique_ptr.h>
#include <bits/shared_ptr.h>

#include <chrono>
#include "../Core/System/Common.h"

#include "../Core/Tools/Stopwatch.h"

//TODO apply https://www.codeproject.com/tips/1068171/cplusplus-custom-rtti
// reson for application because his method is 10x faster than dynamic cast
//TODO implement the following leak detection tool http://wyw.dcweb.cn/leakage.htm
//TODO add the following to the respective classes in teh core 
namespace Library
{
	class RTTI
	{
	public:
		virtual const size_t TypeIdInstance() const = 0;

		virtual RTTI* QueryInterface(const size_t)
		{
			return nullptr;
		}
		virtual const RTTI* QueryInterface(const size_t) const
		{
			return nullptr;
		}

		virtual bool Is(const size_t id) const
		{
			return false;
		}

		virtual bool Is(const std::string& name) const
		{
			return false;
		}

		template <typename T>
		T* As()
		{
			if (Is(T::TypeIdClass()))
			{
				return (T*)this;
			}

			return nullptr;
		}
		template <typename T>
		const T* As() const
		{
			if (Is(T::TypeIdClass()))
			{
				return (T*)this;
			}

			return nullptr;
		}
	};

#define RTTI_DECLARATIONS(Type, ParentType)                            \
    public:                                                            \
        static std::string TypeName() { return std::string(#Type); }   \
        virtual const size_t TypeIdInstance() const                    \
        { return Type::TypeIdClass(); }                                \
        static const size_t TypeIdClass()                              \
        { static int d = 0; return (size_t) &d; }                      \
        virtual Library::RTTI* QueryInterface( const size_t id )       \
        {                                                              \
            if (id == TypeIdClass())                                   \
                { return (RTTI*)this; }                                \
            else                                                       \
                { return ParentType::QueryInterface(id); }             \
        }                                                              \
        virtual const Library::RTTI* QueryInterface( const size_t id ) const \
        {                                                              \
            if (id == TypeIdClass())                                   \
                { return (RTTI*)this; }                                \
            else                                                       \
                { return ParentType::QueryInterface(id); }             \
        }                                                              \
        virtual bool Is(const size_t id) const                         \
        {                                                              \
            if (id == TypeIdClass())                                   \
                { return true; }                                       \
            else                                                       \
                { return ParentType::Is(id); }                         \
        }                                                              \
        virtual bool Is(const std::string& name) const                 \
        {                                                              \
            if (name == TypeName())                                    \
                { return true; }                                       \
            else                                                       \
                { return ParentType::Is(name); }                       \
        }
}


using namespace Library;


class RenderingTime
{
private:
	// total time between each frame
	double m_elapsedTime;
	//total amount of time fro mwhe nthe clock was reset
	double m_TotalTime;

public:

	void UpdateTime(double &p_time) const;
	void Reset();


};


class RenderingClock
{
private:

public:
};


static void DeleteObject(void* myObject)
{
	delete myObject;
}



class RenderingApplication;


class RenderingComponent : public Library::RTTI
{
	RTTI_DECLARATIONS(RenderingComponent, Library::RTTI)

public:
	RenderingComponent(std::string p_componentName);

	RenderingComponent(std::string p_componentName, RenderingApplication *p_renderingApplication);

	~RenderingComponent()
	{}

	bool Enabled() const;
	void SetEnabled(bool p_enabled);

	virtual void Initialise() = 0;
	virtual void Update(const RenderingTime& p_time) = 0;

private:
	bool m_enabled;
	RenderingApplication* m_renderingApplication;
	std::string m_componentName;
};

bool RenderingComponent::Enabled() const
{
	return m_enabled;
}

void RenderingComponent::SetEnabled(bool p_enabled)
{
	m_enabled = p_enabled;
}

RenderingComponent::RenderingComponent(std::string p_componentName) :
	m_componentName(p_componentName),
	m_enabled(true)
{ }


RenderingComponent::RenderingComponent(std::string p_componentName, RenderingApplication *p_renderingApplication):
	m_componentName(p_componentName),
	m_renderingApplication(p_renderingApplication)
{ }


class DrawableRenderingComponent : public  RenderingComponent
{
	RTTI_DECLARATIONS(DrawableRenderingComponent, RenderingComponent)

public:
	DrawableRenderingComponent(std::string p_componentName);

	DrawableRenderingComponent(std::string p_componentName, RenderingApplication *p_renderingApplication):
			RenderingComponent(p_componentName, p_renderingApplication)
	{}

	virtual void Draw(const RenderingTime &p_time);

	bool Visible() const;
	void SetVisible(bool);

private:
	bool m_visible;
};

bool DrawableRenderingComponent::Visible() const
{
	return m_visible;
}

void DrawableRenderingComponent::SetVisible(bool p_visible)
{
	m_visible = p_visible;
}


class RenderingApplication {
public:

	typedef std::function<void(int, int, int, int)> KeyboardHandler;

	RenderingApplication(int, int, std::string);

	virtual void Run();
	void AddKeyHandler(KeyboardHandler p_handler);
	void DeleteKeyHandler(KeyboardHandler p_handler);
	static void OnKey(GLFWwindow *, int, int, int, int);

	void AddComponent(RenderingComponent* p_newComponent);
	static RenderingApplication* s_internalInstance;



protected:
	virtual void InitialiseWindow();
	virtual void InitialiseOpenGL();
	virtual void Initialise();
	virtual void Draw(const RenderingTime&);
	virtual void Update(const RenderingTime&);
	virtual void ShutDown();

	GLFWwindow *m_window;
	std::vector<RenderingComponent*> m_components;

private:

	void CentreWindow();


	std::string m_windowName;
	int m_screenWidth;
	int m_screenHeight;

	int m_openGlMajorVer;
	int m_openGlMinorVer;

	RenderingTime m_renderingTime;

	std::map<KeyboardHandler *, KeyboardHandler> m_keyboardHandlers;


	bool m_depthStencileBufferEnabled = false;

};


RenderingApplication* RenderingApplication::s_internalInstance;


void RenderingApplication::Run()
{
	InitialiseWindow();
	InitialiseOpenGL();
	Initialise();

	do
	{
		Update(m_renderingTime);
		Draw(m_renderingTime);
		glfwPollEvents();

	}while(!glfwWindowShouldClose(m_window));

	ShutDown();
}


RenderingApplication::RenderingApplication(int p_width, int p_height, std::string p_windowName)
		: m_screenWidth(p_width),
		  m_screenHeight(p_height),
		  m_windowName(p_windowName)
{ }



void RenderingApplication::ShutDown()
{
	glfwDestroyWindow(m_window);
	glfwTerminate();
}

void RenderingApplication::InitialiseWindow()
{
	if(glfwInit() == GLFW_FALSE)
	{
		std::cout << "Failed to initialise GLFW" <<std::endl;
		return;
	}

	m_window = glfwCreateWindow(m_screenWidth, m_screenHeight, m_windowName.c_str(), nullptr, nullptr);

	if(m_window != nullptr)
	{
		ShutDown();
		std::cout << "Window Initialised" << std::endl;
	}

	//TODO center window
}

void RenderingApplication::CentreWindow()
{
	Implement("CentreWindow");
}

void RenderingApplication::Initialise()
{
	for(RenderingComponent* currentComponent : m_renderingComponents)
	{
		currentComponent->Initialise();
	}
}

void RenderingApplication::Update(const RenderingTime& p_time)
{
	for(RenderingComponent* currentComponent : m_renderingComponents)
	{
		if(currentComponent->Enabled())
		{
			currentComponent->Update(p_time);
		}
	}

}

void RenderingApplication::Draw(const RenderingTime &p_gameTime)
{
	for(RenderingComponent* currentComponent : m_components)
	{
		 DrawableRenderingComponent* drawableComponent = currentComponent->As<DrawableRenderingComponent>();

		if(drawableComponent != nullptr && drawableComponent->Visible())
		{
			drawableComponent->Draw(m_renderingTime);
		}
	}


}

void RenderingApplication::InitialiseOpenGL()
{
	this->s_internalInstance = this;
	//setting out window as the current context
	glfwMakeContextCurrent(m_window);
	if(glewInit() == GL_FALSE)
	{
		std::cout << "Failed to initalise OpenGl" << std::endl;
	}

	//setting the major and minor versions of opengl
	glGetIntegerv(GL_MAJOR_VERSION, &m_openGlMajorVer);
	glGetIntegerv(GL_MINOR_VERSION, &m_openGlMinorVer);

	//setting the current window to fullscreen
	glViewport(0,0, m_screenWidth, m_screenWidth);

	glfwSetKeyCallback(m_window, RenderingApplication::OnKey);

	if(m_depthStencileBufferEnabled)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_EQUAL);
	}
}

void RenderingApplication::OnKey(GLFWwindow * p_window, int key, int scancode, int action, int mode)
{
	// c++11 syntax (foreach loop kind of) this is an iterator
	for(auto currentHandler : RenderingApplication::s_internalInstance->m_keyboardHandlers)
	{
		currentHandler.second(key, scancode, action, mode);
	}
}

void RenderingApplication::AddKeyHandler(KeyboardHandler p_handler)
{
	m_keyboardHandlers[&p_handler] = p_handler;
}

void RenderingApplication::AddComponent(RenderingComponent* p_newComponent)
{
	m_components.push_back(p_newComponent);
}


class PointDemo : public DrawableRenderingComponent
{
	RTTI_DECLARATIONS(PointDemo, DrawableRenderingComponent)

public:
	PointDemo(std::string p_componentName)
			: DrawableRenderingComponent(p_componentName)
	{}

	PointDemo(std::string p_componentName, RenderingApplication* p_renderingApplication)
			: DrawableRenderingComponent(p_componentName,  p_renderingApplication)
	{}

	virtual void Draw(const RenderingTime& p_renderingTime) override;

	//disabling the copy constructor  and the assignement operator
	PointDemo() = delete;
	PointDemo(const PointDemo& rhs) = delete;
	PointDemo& operator= (const PointDemo& rhs) = delete;

	PointDemo(const PointDemo&& rhs):
			m_ShaderProgram(rhs.m_ShaderProgram),
			m_VertexArrasyObject(rhs.m_VertexArrasyObject),
			DrawableRenderingComponent(rhs)
	{

	}
private:

	GLuint m_ShaderProgram;
	GLuint m_VertexArrasyObject;
};



virtual void PointDemo::Draw(const RenderingTime &p_renderingTime)
{

}

class Lesson2 : public RenderingApplication
{
public:
	Lesson2(int p_width, int p_height, std::string p_title)
			: RenderingApplication(p_width, p_height, p_title)
	{ }

	void OnKey(int p_key, int p_scancode, int p_action, int p_mods);
protected:
	virtual void Initialise() override;
	virtual void Draw(const RenderingTime& p_renderingTime) override;
	virtual void ShutDown() override;

private:


	PointDemo* m_pointDemo;
	KeyboardHandler* m_keyboardHandler;

};


virtual void Lesson2::Initialise()
{

	m_pointDemo = new PointDemo("PointDemo", this);
	AddComponent(m_pointDemo);

	//using the bind consturct to get a function pooint to an instance method
	m_keyboardHandler = std::bind(&Lesson2::OnKey, this, _1,_2,_3,_4);
	AddKeyHandler(m_keyboardHandler);

	//initliase all components
	RenderingApplication::Initialise();
}

virtual void Lesson2::ShutDown()
{
	DeleteObject(m_pointDemo);
	DeleteKeyHandler(m_keyboardHandler);

	RenderingApplication::ShutDown();
}


virtual void Lesson2::Draw(const RenderingTime& p_RenderingTime)
{
	glClearColor(1,0,0,1);
	RenderingApplication::Draw(p_RenderingTime);
	glfwSwapBuffers(m_window);
}

void Lesson2::OnKey(int p_key, int p_scanCode, int p_action, int p_mods)
{
	if(p_key == GLFW_KEY_ESCAPE && p_action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(m_window, GL_TRUE);
	}
}



int main()
{
	std::unique_ptr<RenderingApplication> application(new Lesson2(500, 500, "Lesson2"));
	application->Run();
	return 0;
}